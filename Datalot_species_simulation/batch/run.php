<?php

// For includes since I can't depend on a path to the code. Using a regex hack
// to make includes "absolute" (should work as long as the repo is installed
// normally)
define("APP_BASE_DIRECTORY", "Datalot_species_simulation");

require_once get_quasi_absolute_file_path('provided/spyc.php');
require_once get_quasi_absolute_file_path('api/simulation.php');

$conf = spyc_load_file(get_quasi_absolute_file_path('provided/config.txt'));

$s = new simulation($conf);

$s->run();

function get_quasi_absolute_file_path($rel_path)
{
    return preg_replace("/(.*\/" . APP_BASE_DIRECTORY . "\/).*/", "$1", __FILE__)
        . $rel_path;
}
?>
