To run, execute batch/run.php from anywhere.

Tried to stay within the 80 character rule, made formatting a little crazy in places.

Assumptions, in no particular order
-----------------------------------
- Each simulation begins in January
- There is no concept of infant animals, they are instantly adults
- All animals are 0 years old when the simulation starts
- Each mating is 100% successful at impregnation
- Newborn animals consume food and water during the month they are born
- Males will not mate with a pregnant female
- Males can mate an unlimited number of times in a month
- An animal consuming less than its indicated food and/or water (but more than 0) still counts as being nourished
- "One full month above or below threshold for species" means two consecutive month checks where temp is out of bounds...
  otherwise everyone would die everytime the temperature dipped.. instead, everyone except for newborns dies.. still seems
  pretty harsh
- "Average population" is just [sum of population count each month] / [number of months]
