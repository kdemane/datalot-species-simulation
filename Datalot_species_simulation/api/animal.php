<?php

require_once get_quasi_absolute_file_path('api/constants.php');

class animal
{
    private $attributes                       // config vars
          , $gender // 'F' or 'M'

          , $age_in_months                    // operation vars
          , $num_months_pregnant
          , $num_months_without_food
          , $num_months_without_water
          , $num_months_temperature_extreme;

    public function __construct($attributes, $gender)
    {
        $this->attributes                     = $attributes;

        $this->age_in_months                  = 0;
        $this->gender                         = $gender;

        $this->num_months_pregnant            = NULL;

        $this->num_months_without_food        =
        $this->num_months_without_water       =
        $this->num_months_temperature_extreme = 0;
    }

    public function get_gender()
    {
        return $this->gender;
    }

    public function age()
    {
        $this->age_in_months++;

        if ($this->num_months_pregnant !== NULL)
            $this->num_months_pregnant++;
    }

    public function is_dead($temperature)
    {
        if (get_years($this->age_in_months) > $this->attributes['life_span'])
            return CAUSE_OF_DEATH_AGE;

        if ($this->num_months_without_food == 3)
            return CAUSE_OF_DEATH_STARVATION;

        if ($this->num_months_without_water == 1)
            return CAUSE_OF_DEATH_THIRST;

        if ($this->num_months_temperature_extreme == 1)
        {
            if ($this->_is_temperature_extreme($temperature) === -1)
                return CAUSE_OF_DEATH_COLD;

            if ($this->_is_temperature_extreme($temperature) === 1)
                return CAUSE_OF_DEATH_HEAT;
        }

        return FALSE;
    }

    // Are we giving birth or getting pregnant? return TRUE for giving birth.
    // Otherwise just record pregnancy if it happens
    public function evaluate_pregnancy($num_males, $is_habitat_supportive)
    {
        if ($this->gender == 'F')
        {
            if ($this->num_months_pregnant !== NULL)
            {
                if ($this->num_months_pregnant ==
                    $this->attributes['gestation_period'])
                {
                    $this->num_months_pregnant = NULL;

                    return TRUE;
                }
            }
            else if ($num_males &&
                     ($is_habitat_supportive || check_percent(0.5)) &&
                     (get_years($this->age_in_months) >=
                      $this->attributes['minimum_breeding_age']))
            {
                $this->num_months_pregnant = 0;
            }
        }

        return FALSE;
    }

    public function nourish(&$food, &$water)
    {
        $this->_consume_resource(
            $food,
            $this->attributes['monthly_food_consumption'],
            $this->num_months_without_food);

        $this->_consume_resource(
            $water,
            $this->attributes['monthly_water_consumption'],
            $this->num_months_without_water);
    }

    private function _consume_resource(&$resource,
                                       $consumption,
                                       &$resource_danger_month_tracker)
    {
        if ($resource)
        {
            $resource -= $consumption;

            if ($resource < 0)
                $resource = 0;

            $resource_danger_month_tracker = 0;

            return TRUE;
        }

        $resource_danger_month_tracker++;

        return FALSE;
    }

    public function endure_temperature($temperature)
    {
        if ($this->_is_temperature_extreme($temperature))
            $this->num_months_temperature_extreme++;
        else
            $this->num_months_temperature_extreme = 0;
    }

    // return -1 for cold, 1 for hot, 0 for no
    private function _is_temperature_extreme($temperature)
    {
        if ($temperature < $this->attributes['minimum_temperature'])
            return -1;

        if ($temperature > $this->attributes['maximum_temperature'])
            return 1;

        return 0;
    }
}
?>
