<?php

require_once get_quasi_absolute_file_path('api/animal.php');
require_once get_quasi_absolute_file_path('api/habitat.php');

class species
{
    protected $name            // config vars
            , $attributes

            , $habitat         // operation vars
            , $animals
            , $habitat_status
            , $recent_births
            , $recent_deaths;

    public function __construct($species_conf, $habitat_conf)
    {
        $this->name               = $species_conf['name'];
        $this->species_attributes = $species_conf['attributes'];

        $this->habitat            = new habitat($habitat_conf);

        $this->num_males          = 1;

        $this->animals   = array(new animal($this->species_attributes, 'F'),
                                 new animal($this->species_attributes, 'M'));
    }

    public function get_population()
    {
        return count($this->animals);
    }

    public function get_recent_births()
    {
        return $this->recent_births;
    }

    public function get_recent_deaths()
    {
        return $this->recent_deaths;
    }

    public function elapse_month($month)
    {
        $this->recent_births = 0;
        $this->recent_deaths = array();

        $this->habitat->elapse_month($month);

        $this->habitat_status = $this->habitat->get_status();

        foreach ($this->animals as $i => $a)
        {
            $a->age();

            // it's been a month.. who died
            if ($this->_is_dead($a))
            {
                unset($this->animals[$i]);

                continue;
            }

            $this->_live($a);

            if ($gender = $this->_evaluate_pregnancy($a))
            {
                // a child is born...
                $n = new animal($this->species_attributes, $gender);

                // newborns need to eat and drink
                $this->_live($n);

                // and can be impregnated, but obviously cannot give birth
                // this month
                $this->_evaluate_pregnancy($n);

                $this->animals[] = $n;
            }
        }
    }

    private function _live(&$a)
    {
        $a->nourish($this->habitat_status['food'],
                    $this->habitat_status['water']);

        $a->endure_temperature($this->habitat_status['temperature']);
    }

    private function _evaluate_pregnancy(&$a)
    {
        if ($a->evaluate_pregnancy($this->num_males,
                                   $this->_is_habitat_supportive()))
        {
            if (check_percent(50))
            {
                $gender = 'F';
            }
            else
            {
                $gender = 'M';
                $this->num_males++;
            }

            $this->recent_births++;

            return $gender;
        }

        return FALSE;
    }

    private function _is_dead(&$a)
    {
        if ($cause_of_death = $a->is_dead($this->habitat_status['temperature']))
        {
            $this->_record_death($cause_of_death);

            if ($a->get_gender() == 'M')
                $this->num_males--;

            return TRUE;
        }

        return FALSE;
    }

    private function _record_death($cause_of_death)
    {
        if (isset($this->recent_deaths[$cause_of_death]))
            $this->recent_deaths[$cause_of_death]++;
        else
            $this->recent_deaths[$cause_of_death] = 1;
    }

    private function _is_habitat_supportive()
    {
        $num_animals = count($this->animals);

        return (($this->habitat_status['food'] >
                 ($num_animals *
                  $this->species_attributes['monthly_food_consumption'])) &&
                ($this->habitat_status['water'] >
                 ($num_animals *
                  $this->species_attributes['monthly_water_consumption'])));
    }
}
?>
