<?php

require_once get_quasi_absolute_file_path('api/iteration.php');

/**
 * The overall, complete simulation, all iterations of all permutations of
 * species and habitat.
 **/
class simulation
{
    private $years       // config vars
          , $iterations
          , $species
          , $habitats
          , $indent = "        "

          , $stats;      // operation vars

    public function __construct($conf)
    {
        $this->years      = $conf['years'];
        $this->iterations = $conf['iterations'];
        $this->species    = $conf['species'];
        $this->habitats   = $conf['habitats'];
    }

    public function run()
    {
        for ($i = 0; $i < $this->iterations; $i++)
        {
            foreach ($this->species as $species)
            {
                foreach ($this->habitats as $habitat)
                {
                    $itr = new iteration($this->years,
                                         $species,
                                         $habitat);

                    if ($itr->run())
                    {
                        $this->_collect_stats($species['name'],
                                              $habitat['name'],
                                              $itr->get_final_stats());
                    }
                }
            }
        }

        $this->_print_output();
    }

    private function _collect_stats($species, $habitat, $stats)
    {
        if (!isset($this->stats[$species][$habitat]))
            $this->_init_stats($species, $habitat);

        $this->stats[$species][$habitat]['average_population'] +=
            $stats['average_population'];

        $this->stats[$species][$habitat]['max_population'] +=
            $stats['max_population'];

        $this->stats[$species][$habitat]['mortality_rate'] +=
            $stats['mortality_rate'];

        foreach (array_keys($stats['deaths']) as $cause_of_death)
        {
            $num_deaths = $stats['deaths'][$cause_of_death];

            $this->stats[$species][$habitat]['deaths'][$cause_of_death]
                += $num_deaths;

            $this->stats[$species][$habitat]['death_toll'] += $num_deaths;
        }
    }

    private function _init_stats($species, $habitat)
    {
        $this->stats[$species][$habitat] =
            array('average_population' => 0,
                  'max_population'     => 0,
                  'mortality_rate'     => 0,
                  'death_toll'         => 0,
                  'deaths'             =>
                      array(CAUSE_OF_DEATH_STARVATION => 0,
                            CAUSE_OF_DEATH_AGE        => 0,
                            CAUSE_OF_DEATH_THIRST     => 0,
                            CAUSE_OF_DEATH_COLD       => 0,
                            CAUSE_OF_DEATH_HEAT       => 0));

        // following sample output order of causes of death

    }

    private function _print_output()
    {
        print "Simulation ran for "
            . $this->iterations
            . " iteration" . ($this->iterations > 1 ? 's' : '')
            . " at " . $this->years . " years per iteration\n";

        foreach ($this->stats as $species => $habitats)
        {
            $this->_print_stat(0, $species . ':');

            foreach ($habitats as $habitat => $stats)
            {
                $this->_print_stat(1, $habitat . ':');

                $this->_print_stats($stats);
            }
        }
    }

    private function _print_stats($stats)
    {
        $this->_print_stat(2, 'Average Population: ' .
                           round($stats['average_population'] /
                                 $this->iterations));

        $this->_print_stat(2, 'Max Population: ' .
                           round($stats['max_population'] /
                                 $this->iterations));

        $this->_print_stat(2, 'Mortality Rate: ' .
                           number_format(
                               round(100 * ($stats['mortality_rate'] /
                                            $this->iterations), 2), 2) . '%');

        $this->_print_stat(2, 'Cause of Death:');

        foreach ($stats['deaths'] as $cause => $num_deaths)
            $this->_print_stat(
                3,
                sprintf("% 8s",
                        number_format(
                            round(100 *
                                  ($num_deaths /
                                   $stats['death_toll']), 2), 2) . '% ')
                . $cause);
    }

    private function _print_stat($num_indents, $stat)
    {
        for ($i = 0; $i < $num_indents; $i++)
            print $this->indent;

        print $stat . "\n";
    }
}
?>
