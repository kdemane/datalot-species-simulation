<?php

require_once get_quasi_absolute_file_path('api/constants.php');
require_once get_quasi_absolute_file_path('api/species.php');

// Complete run of one species in one habitat
class iteration
{
    private $years             // config vars
          , $species

          , $month             // operation vars

          , $max_population    // stats
          , $sum_population
          , $total_population
          , $deaths
          , $death_toll;

    public function __construct($years, $species, $habitat)
    {
        $this->years            = $years;

        $this->species          = new species($species, $habitat);

        $this->month            = 1;

        $this->max_population   =
        $this->total_population = $this->species->get_population();

        $this->death_toll       = 0;

        $this->deaths           = array();
    }

    public function run()
    {
        while (get_years($this->month) < $this->years)
            $this->_elapse_month();

        return TRUE;
    }

    private function _elapse_month()
    {
        $this->month++;

        $this->species->elapse_month($this->month % 12);

        $this->_tally_stats();
    }

    private function _tally_stats()
    {
        $cur_pop = $this->species->get_population();

        $this->sum_population += $cur_pop;

        if ($cur_pop > $this->max_population)
            $this->max_population = $cur_pop;

        $this->total_population += $this->species->get_recent_births();

        if ($deaths = $this->species->get_recent_deaths())
        {
            foreach ($deaths as $death_type => $count)
            {
                if (isset($this->deaths[$death_type]))
                    $this->deaths[$death_type] += $count;
                else
                    $this->deaths[$death_type] = $count;

                $this->death_toll += $count;
            }
        }
    }

    public function get_final_stats()
    {
        return array('average_population' => round($this->sum_population /
                                                   $this->month),
                     'max_population'     => $this->max_population,
                     'mortality_rate'     => $this->death_toll /
                                             $this->total_population,
                     'deaths'             => $this->deaths);
    }
}

?>
