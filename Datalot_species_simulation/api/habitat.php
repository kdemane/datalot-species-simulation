<?php

require_once get_quasi_absolute_file_path('api/util.php');

class habitat
{
    private $name           // config vars
          , $monthly_food
          , $monthly_water
          , $average_temps

          , $cur_temp;      // operation vars

    public function __construct($conf)
    {
        $this->name          = $conf['name'];
        $this->monthly_food  = $conf['monthly_food'];
        $this->monthly_water = $conf['monthly_water'];
        $this->average_temps = $conf['average_temperature'];

        // always will be inited in January
        $this->cur_temp      = $this->_calculate_temp(1);
    }

    // refresh, food and water always replenish
    public function get_status()
    {
        return array('food'        => $this->monthly_food,
                     'temperature' => $this->cur_temp,
                     'water'       => $this->monthly_water);
    }

    public function elapse_month($month)
    {
        $this->cur_temp  = $this->_calculate_temp($month);
    }

    private function _calculate_temp($month)
    {
        $base_temp = $this->average_temps[get_season($month)];

        $max_fluctuation = check_percent(0.5) ? 15 : 5;

        return $base_temp + mt_rand(-$max_fluctuation, $max_fluctuation);
    }
}

?>
