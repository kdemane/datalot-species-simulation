<?php

// Gotta go above 100 for decimal precision %'s. This function will only
// handle 1 digit of decimal precision but that's all that is needed for the
// test
function check_percent($percent)
{
    return mt_rand(1, 1000) <= (10 * $percent);
}

function get_season($month)
{
    switch($month)
    {
    case 0: // for modulus loop
    case 1:
    case 2:
    case 12:
        return 'winter';
        break;

    case 3:
    case 4:
    case 5:
        return 'spring';
        break;

    case 6:
    case 7:
    case 8:
        return 'summer';
        break;

    case 9:
    case 10:
    case 11:
        return 'fall';
    }
}

function get_years($months)
{
    return floor($months / 12);
}
?>
